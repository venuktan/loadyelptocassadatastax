package com.cloudwick.cassandra.yelp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LoadBusinessToCassa {

	static Cluster cluster;
	static Session session;
	static String InsertStatement;
	static int i=0;

	public static void main(String[] args) throws FileNotFoundException,
			IOException, ParseException {

		 LoadBusinessToCassa loadBusinessToCassa = new LoadBusinessToCassa();
//		 LoadSData.loadBusinessData("/Users/venuktangirala/sas/yelp_phoenix_academic_dataset/yelp_academic_dataset_business.json");
		 loadBusinessToCassa.connect("127.0.0.1");
		 loadBusinessToCassa.parseLoad("/Users/venuktangirala/yelp_phoenix_academic_dataset/clean/business.json");
		 loadBusinessToCassa.close();
	}

	public  void connect(String node) {
		cluster = Cluster.builder().addContactPoint(node).build();
		com.datastax.driver.core.Metadata metadata = cluster.getMetadata();
		System.out.printf("Connected to cluster: %s\n",
				metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n",
					host.getDatacenter(), host.getAddress(), host.getRack());
		}
		session = cluster.connect();
	}
	
	public void close() {
		cluster.shutdown();
	}


	public void loadData() {

	}

	public  void parseLoad(String jsonFile) throws FileNotFoundException, IOException,
			ParseException {
		try{
			
			
			ObjectMapper objectMapper = new ObjectMapper();
			JsonFactory jsonFactory = objectMapper.getFactory();
			JsonParser jsonParser = jsonFactory
					.createParser(new File(jsonFile));
	
			JsonNode jsonNode = objectMapper.readTree(jsonParser);
			JsonNode businessList = jsonNode.get("business");
			System.out.println(businessList.get(0).get("categories").get(0));
	
			for (JsonNode business : businessList) {
	
				System.out.println(business.get("business_id"));
	
				JsonNode categories = business.get("categories");
	
				StringBuilder holderCategories = new StringBuilder();
				holderCategories.append("[");
	
				for (i = 0; categories.has(i); i++) {
	
					if (i != 0) {
						holderCategories.append(",");
					}
	
					holderCategories.append("'"
							+ categories.get(i).toString().replace("'", "").replace("\"", "")
									.replace("\\n", "") + "'");
				}
				holderCategories.append("],");
	
				JsonNode neighborhoods = business.get("neighborhoods");
	
				StringBuilder holderNeighborhoods = new StringBuilder();
//				if (neighborhoods.)
				holderNeighborhoods.append("[");
				for (int i = 0; neighborhoods.has(i); i++) {
	
					if (i != 0) {
						holderNeighborhoods.append(",");
					}
	
					holderNeighborhoods.append("'"
							+ neighborhoods.get(i).toString().replace("'", "").replace("\"", "")
									.replace("\\n", "") + "'");
				}
				holderNeighborhoods.append("],");
	
				StringBuilder stringBuilder = new StringBuilder();
	
				stringBuilder.append("INSERT INTO yelp.business");
				stringBuilder
						.append(" (business_id,categories ,city ,full_address ,latitude,longitude, name, neighborhoods, open, review_count, stars, state, type) ");
				stringBuilder.append("VALUES(");
				stringBuilder.append("'"
						+ business.get("business_id").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				stringBuilder.append(holderCategories.toString());
				stringBuilder.append("'"
						+ business.get("city").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				stringBuilder.append("'"
						+ business.get("full_address").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				stringBuilder.append(Double.parseDouble(business.get("latitude")
						.toString().replace("'", "").replace("\\n", "").replace("\"", ""))
						+ ",");
				stringBuilder.append(Double.parseDouble(business.get("longitude")
						.toString().replace("'", "").replace("\\n", "").replace("\"", ""))
						+ ",");
				stringBuilder.append("'"
						+ business.get("name").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				stringBuilder.append(holderNeighborhoods.toString());
				stringBuilder.append(business.get("open").toString()
						.replace("'", "").replace("\\n", "").replace("\"", "")
						+ ",");
				stringBuilder.append(Integer.parseInt(business.get("review_count")
						.toString().replace("'", "").replace("\\n", "").replace("\"", ""))
						+ ",");
				stringBuilder.append(Float.parseFloat(business.get("stars")
						.toString().replace("'", "").replace("\\n", "").replace("\"", ""))
						+ ",");
				stringBuilder.append("'"
						+ business.get("state").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				stringBuilder.append("'"
						+ business.get("type").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "'");
				stringBuilder.append(");");
				
				InsertStatement=stringBuilder.toString();
				session.execute(stringBuilder.toString());
				System.out.println("loaded business"+i);
	
			}

		}catch(NoHostAvailableException e){
			
			e.printStackTrace();
			
			System.out.println(e.getMessage());
			System.out.println(InsertStatement);
			System.out.println("Failed @ : " + i);
		}catch (Exception e) {
			
			e.printStackTrace();
			System.out.println(InsertStatement);
			System.out.println("Failed @ : : " + i);
		}

	}

}
