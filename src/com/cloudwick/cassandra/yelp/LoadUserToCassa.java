package com.cloudwick.cassandra.yelp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LoadUserToCassa {

	static Cluster cluster;
	static Session session;
	static String InsertStatement;
	static int i=0;

	public static void main(String[] args) throws FileNotFoundException,
			IOException, ParseException {

		LoadUserToCassa loadUserToCassa = new LoadUserToCassa();
//		 LoadSData.loadBusinessData("/Users/venuktangirala/sas/yelp_phoenix_academic_dataset/yelp_academic_dataset_business.json");
		 loadUserToCassa.connect("127.0.0.1");
		 loadUserToCassa.parseLoad("/Users/venuktangirala/yelp_phoenix_academic_dataset/clean/user.json");
		 loadUserToCassa.close();
	}

	public  void connect(String node) {
		cluster = Cluster.builder().addContactPoint(node).build();
		com.datastax.driver.core.Metadata metadata = cluster.getMetadata();
		System.out.printf("Connected to cluster: %s\n",
				metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n",
					host.getDatacenter(), host.getAddress(), host.getRack());
		}
		session = cluster.connect();
	}
	
	public void close() {
		cluster.shutdown();
	}


	public void loadData() {

	}

	public  void parseLoad(String jsonFile) throws FileNotFoundException, IOException,
			ParseException {
		try{
			
			
			ObjectMapper objectMapper = new ObjectMapper();
			JsonFactory jsonFactory = objectMapper.getFactory();
			JsonParser jsonParser = jsonFactory
					.createParser(new File(jsonFile));
	
			JsonNode jsonNode = objectMapper.readTree(jsonParser);
			JsonNode userList = jsonNode.get("user");
//			System.out.println(userList.get(0).get("user_id").get(0));
	
			for (JsonNode user : userList) {
		
				StringBuilder stringBuilder = new StringBuilder();
	
				stringBuilder.append("INSERT INTO yelp.user");
				stringBuilder
						.append(" (name, average_stars , review_count, type , user_id, votes)");
				stringBuilder.append("VALUES(");
				
				stringBuilder.append("'"
						+ user.get("name").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				
				stringBuilder.append( Float.parseFloat( user.get("average_stars").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") ) +",");
				
				stringBuilder.append(Integer.parseInt(user.get("review_count").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") ) +",");
				
				
				stringBuilder.append("'"
						+ user.get("type").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				
				stringBuilder.append("'"+user.get("user_id").toString()
						.replace("'", "").replace("\\n", "").replace("\"", "")
						+ "',");
				
				stringBuilder.append("{'cool':" + Integer.parseInt(user.get("votes").get("cool").toString() ) 
									+",'funny':"+Integer.parseInt(user.get("votes").get("funny").toString() )
									+",'useful':"+Integer.parseInt(user.get("votes").get("useful").toString() )
									+"}"
									);				
				
				stringBuilder.append(");");
				
				InsertStatement=stringBuilder.toString();
				session.execute(stringBuilder.toString());
				
				System.out.println("loaded user"+ user.get("user_id"));
	
			}

		}catch(NoHostAvailableException e){
			
			e.printStackTrace();
			
			System.out.println(e.getMessage());
			System.out.println(InsertStatement);
			System.out.println("Failed @ : " + i);
		}catch (Exception e) {
			
			e.printStackTrace();
			System.out.println(InsertStatement);
			System.out.println("Failed @ : : " + i);
		}

	}

	
}
