package com.cloudwick.cassandra.yelp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
//import java.sql.Date;
import java.text.ParseException;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LoadReviewToCassa {

	static Cluster cluster;
	static Session session;
	static String InsertStatement;
	static int i=0;

	public static void main(String[] args) throws FileNotFoundException,
			IOException, ParseException {

		LoadReviewToCassa loadUserToCassa = new LoadReviewToCassa();
//		 LoadSData.loadBusinessData("/Users/venuktangirala/sas/yelp_phoenix_academic_dataset/yelp_academic_dataset_business.json");
		 loadUserToCassa.connect("127.0.0.1");
		 loadUserToCassa.parseLoad("/Users/venuktangirala/yelp_phoenix_academic_dataset/clean/checkin.json");
		 loadUserToCassa.close();
	}

	public  void connect(String node) {
		cluster = Cluster.builder().addContactPoint(node).build();
		com.datastax.driver.core.Metadata metadata = cluster.getMetadata();
		System.out.printf("Connected to cluster: %s\n",
				metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n",
					host.getDatacenter(), host.getAddress(), host.getRack());
		}
		session = cluster.connect();
	}
	
	public void close() {
		cluster.shutdown();
	}


	public void loadData() {

	}

	public  void parseLoad(String jsonFile) throws FileNotFoundException, IOException,
			ParseException {
		try{
			
			
			ObjectMapper objectMapper = new ObjectMapper();
			JsonFactory jsonFactory = objectMapper.getFactory();
			JsonParser jsonParser = jsonFactory
					.createParser(new File(jsonFile));
	
			JsonNode jsonNode = objectMapper.readTree(jsonParser);
			JsonNode checkin_info = jsonNode.get("checkin");
//			System.out.println(userList.get(0).get("user_id").get(0));
	
			for (JsonNode checkin : checkin_info) {
		
				StringBuilder stringBuilder = new StringBuilder();
	
				stringBuilder.append("INSERT INTO yelp.checkin");
				
				stringBuilder.append(" (business_id , type, checkin_info)");
				
				stringBuilder.append("VALUES(");
								
				stringBuilder.append("'"
						+ checkin.get("business_id").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				
				stringBuilder.append("'"
						+ checkin.get("type").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
			
				stringBuilder.append(checkin.get("checkin_info").toString().replace("\"", "'")
						.replace("\\n", "") );
				
				stringBuilder.append(");");
				
				InsertStatement=stringBuilder.toString();
				session.execute(stringBuilder.toString());
				
				System.out.println("loaded review"+ checkin.get("business_id"));
	
			}

		}catch(NoHostAvailableException e){
			
			e.printStackTrace();
			
			System.out.println(e.getMessage());
			System.out.println(InsertStatement);
			System.out.println("Failed @ : " + i);
		}catch (Exception e) {
			
			e.printStackTrace();
			System.out.println(InsertStatement);
			System.out.println("Failed @ : : " + i);
		}

	}

	
}
