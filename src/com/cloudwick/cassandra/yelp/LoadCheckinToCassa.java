package com.cloudwick.cassandra.yelp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

public class LoadCheckinToCassa {

	static Cluster cluster;
	static Session session;
	static String InsertStatement;
	static int i=0;
//	static Logger logger = Logger.getLogger(LoadCheckinToCassa.class);
	
	public static void main(String[] args) throws FileNotFoundException,
			IOException, ParseException {

		
		LoadCheckinToCassa loadCheckinToCassa = new LoadCheckinToCassa();
//		 LoadSData.loadBusinessData("/Users/venuktangirala/sas/yelp_phoenix_academic_dataset/yelp_academic_dataset_business.json");
		 loadCheckinToCassa.connect("127.0.0.1");
		 loadCheckinToCassa.parseLoad("/Users/venuktangirala/yelp_phoenix_academic_dataset/clean/checkin.json");
		 loadCheckinToCassa.close();
	}

	public  void connect(String node) {
		cluster = Cluster.builder().addContactPoint(node).build();
		com.datastax.driver.core.Metadata metadata = cluster.getMetadata();
		
//		logger.warn("connecting");
		
		System.out.printf("Connected to cluster: %s\n",
				metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n",
					host.getDatacenter(), host.getAddress(), host.getRack());
		}
		session = cluster.connect();
	}
	
	public void close() {
		cluster.shutdown();
	}


	public void loadData() {

	}

	public  void parseLoad(String jsonFile) throws FileNotFoundException, IOException,
			ParseException {
		try{
			
			
			ObjectMapper objectMapper = new ObjectMapper();
			JsonFactory jsonFactory = objectMapper.getFactory();
			JsonParser jsonParser = jsonFactory
					.createParser(new File(jsonFile));
	
			JsonNode jsonNode = objectMapper.readTree(jsonParser);
			JsonNode checkinList = jsonNode.get("user");
//			System.out.println(userList.get(0).get("user_id").get(0));
	
			for (JsonNode checkin : checkinList) {
		
				StringBuilder stringBuilder = new StringBuilder();
	
				stringBuilder.append("INSERT INTO yelp.user");
				stringBuilder
						.append(" (name, average_stars , review_count, type , user_id, votes)");
				stringBuilder.append("VALUES(");
				
				stringBuilder.append("'"
						+ checkin.get("name").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				
				stringBuilder.append( Float.parseFloat( checkin.get("average_stars").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") ) +",");
				
				stringBuilder.append(Integer.parseInt(checkin.get("review_count").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") ) +",");
				
				
				stringBuilder.append("'"
						+ checkin.get("type").toString().replace("'", "").replace("\"", "")
								.replace("\\n", "") + "',");
				
				stringBuilder.append("'"+checkin.get("user_id").toString()
						.replace("'", "").replace("\\n", "").replace("\"", "")
						+ "',");
				
				stringBuilder.append("{'cool':" + Integer.parseInt(checkin.get("votes").get("cool").toString() ) 
									+",'funny':"+Integer.parseInt(checkin.get("votes").get("funny").toString() )
									+",'useful':"+Integer.parseInt(checkin.get("votes").get("useful").toString() )
									+"}"
									);				
				
				stringBuilder.append(");");
				
				InsertStatement=stringBuilder.toString();
				session.execute(stringBuilder.toString());
				
//				logger.info("loaded checkin"+checkin.get("user_id"));
				System.out.println("loaded checkin"+ checkin.get("user_id"));
	
			}

		}catch(NoHostAvailableException e){
			
			e.printStackTrace();
			
			System.out.println(e.getMessage());
			System.out.println(InsertStatement);
			System.out.println("Failed @ : " + i);
		}catch (Exception e) {
			
			e.printStackTrace();
			System.out.println(InsertStatement);
			System.out.println("Failed @ : : " + i);
		}

	}

	
}
