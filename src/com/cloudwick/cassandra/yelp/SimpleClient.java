//import org.apache.cassandra.cql3.ResultSet.Metadata;
package com.cloudwick.cassandra.yelp;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.cassandra.cli.CliParser.newColumnFamily_return;
import org.apache.cassandra.cli.CliParser.password_return;
//import org.codehaus.jackson.JsonFactory;
//import org.codehaus.jackson.JsonParser;
//import org.codehaus.jackson.map.ObjectMapper;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SimpleClient {

	private Cluster cluster;
	private Session session;

	public void connect(String node) {
		cluster = Cluster.builder().addContactPoint(node).build();
		com.datastax.driver.core.Metadata metadata = cluster.getMetadata();
		System.out.printf("Connected to cluster: %s\n",
				metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n",
					host.getDatacenter(), host.getAddress(), host.getRack());
		}
		session = cluster.connect();
	}

	public void createSchema() {
		session.execute("CREATE KEYSPACE simplex WITH replication "
				+ "= {'class':'SimpleStrategy', 'replication_factor':3};");

		session.execute("CREATE TABLE simplex.songs (" + "id uuid PRIMARY KEY,"
				+ "title text," + "album text," + "artist text,"
				+ "tags set<text>," + "data blob" + ");");
		session.execute("CREATE TABLE simplex.playlists (" + "id uuid,"
				+ "title text," + "album text, " + "artist text,"
				+ "song_id uuid," + "PRIMARY KEY (id, title, album, artist)"
				+ ");");

	}

	public void close() {
		cluster.shutdown();
	}

	public void loadDataSong() {
		session.execute("INSERT INTO simplex.songs (id, title, album, artist, tags) "
				+ "VALUES ("
				+ "756716f7-2e54-4715-9f00-91dcbea6cf50,"
				+ "'La Petite Tonkinoise',"
				+ "'Bye Bye Blackbird',"
				+ "'Jos�phine Baker'," + "{'jazz', '2013'})" + ";");

		session.execute("INSERT INTO simplex.playlists (id, song_id, title, album, artist) "
				+ "VALUES ("
				+ "2cc9ccb7-6221-4ccb-8387-f22b6a1b354d,"
				+ "756716f7-2e54-4715-9f00-91dcbea6cf50,"
				+ "'La Petite Tonkinoise',"
				+ "'Bye Bye Blackbird',"
				+ "'Jos�phine Baker'" + ");");

	}

	public void loadData() {
		session.execute("INSERT INTO yelp.business  (business_id,categories ,city ,full_address ,latitude,longitude, name, neighborhoods, open, review_count, stars, state, type) VALUES ( 'lolsvenuktangirala' , [ ' Accountants' ,'papa','kaka'], 'Peoria', '8466 W Peoria Ave\nSte 6\nPeoria, AZ 85345', 33.581867, -112.241596, 'Peoria Income Tax Service', ['Hyderabad'], true, 5, 5, 'AZ', 'business');"

		);
	}

	public void querySchema() {

		// ResultSet results =
		// session.execute("SELECT * FROM simplex.playlists; ");
		// for (Row row : results)
		// System.out.println(row.toString());

		ResultSet results = session.execute("SELECT * FROM yelp.business; ");
		for (Row row : results) {
			System.out.println(row.toString());
			System.out.println(String.format("%-30s\t%-20s\t%-20s",
					row.getString("business_id"),
					row.getString("full_address"), row.getString("city")));
		}

	}

	public void parseJson() throws FileNotFoundException, IOException,
			ParseException {
		// ObjectMapper mapper = new ObjectMapper();
		// JsonFactory jsonFactory = new JsonFactory();
		JSONParser jsonParser = new JSONParser();

		try {
			Object object = jsonParser
					.parse(new FileReader(
							"/Users/venuktangirala/yelp_phoenix_academic_dataset/clean/business.json"));
			JSONObject jsonObject = (JSONObject) object;
			String business_id = (String) jsonObject.get("business_id");
		} finally {

		}
	}

	public static void main(String[] args) {
		SimpleClient client = new SimpleClient();
		client.connect("127.0.0.1");
		client.loadData();

		// client.createSchema();
		// client.loadDataSong();
//		client.querySchema();
		
		client.close();

	}

}
